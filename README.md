# team

## Project setup

```
npm install
```

on src/api.js file update baseURL's value to your localhost URL

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```
