import Vue from "vue";
import Vuex from "vuex";
import Api from "./api";
import router from "./router";

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true, //TODO: remove for production
  state: {
    users: []
  },
  mutations: {
    SET_USERS: (state, users) => {
      state.users = users;
    }
  },
  actions: {
    async GET_USERS({ commit }) {
      await Api()
        .get("/users")
        .then(response => {
          console.log(response);
          commit("SET_USERS", response.data.data);
        })
        .catch(() => {
          router.push({ path: "/error" });
        });
    },
    async CREATE_USER({ commit }, data) {
      await Api()
        .post(`/users`, data)
        .then(response => {
          console.log(response);
          commit("SET_USERS", response.data.data);
          router.push({ path: "/" });
        })
        .catch(() => {
          router.push({ path: "/error" });
        });
    },
    async UPDATE_USER({ commit }, data) {
      const userData = {
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email
      };
      await Api()
        .put(`/users/${data._id}`, userData)
        .then(response => {
          console.log(response);
          commit("SET_USERS", response.data.data);
          router.push({ path: "/" });
        })
        .catch(() => {
          router.push({ path: "/error" });
        });
    },
    async DELETE_USER({ commit }, id) {
      await Api()
        .delete(`/users/${id}`)
        .then(response => {
          console.log(response);
          commit("SET_USERS", response.data.data);
          router.push({ path: "/" });
        })
        .catch(() => {
          router.push({ path: "/error" });
        });
    }
  },
  getters: {
    USERS: state => state.users
  }
});
