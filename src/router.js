import Vue from "vue";
import Router from "vue-router";
import Users from "./views/Users.vue";
import CreateUser from "./views/CreateUser.vue";
import EditUser from "./views/EditUser.vue";
import ErrorPage from "./views/Error.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      redirect: "/users"
    },
    {
      path: "/users",
      name: "users",
      component: Users
    },
    {
      path: "/users/create",
      name: "createUser",
      component: CreateUser
    },
    {
      path: "/users/:id",
      name: "editUser",
      component: EditUser
    },
    {
      path: "/error",
      name: "error",
      component: ErrorPage
    },
    {
      path: "*",
      redirect: "/error"
    }
  ]
});
