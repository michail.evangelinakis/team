import axios from "axios";

export default () => {
  return axios.create({
    baseURL: "http://localhost:8080", //TODO: set with your localhost URL
    withCredentials: false,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  });
};
